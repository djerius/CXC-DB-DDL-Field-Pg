# NAME

CXC::DB::DDL::Field::Pg - DBD::Pg specific Field class

# VERSION

version 0.17

# METHODS

## type\_name

    $name = $field->type_name( $dbh );

return the type name for the type

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-cxc-db-ddl-field-pg@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-DB-DDL-Field-Pg](https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-DB-DDL-Field-Pg)

## Source

Source is available at

    https://gitlab.com/djerius/cxc-db-ddl-field-pg

and may be cloned from

    https://gitlab.com/djerius/cxc-db-ddl-field-pg.git

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2024 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
