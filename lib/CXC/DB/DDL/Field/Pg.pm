package CXC::DB::DDL::Field::Pg;

# ABSTRACT: DBD::Pg specific Field class

use v5.26;
our $VERSION = '0.17';
use experimental 'signatures';

use Scalar::Util ();
use Ref::Util    ();

package    ## no critic (Modules::ProhibitMultiplePackages)
  CXC::DB::DDL::Field::PgType {
    use base 'CXC::DB::DDL::FieldType';

    sub pg_type_name ( $self ) { $self->name =~ s/^PG_//ri; }
}

use CXC::DB::DDL::Util 0.15 {
    add_dbd => {
        dbd         => 'Pg',
        tag         => ':pg_types',
        field_class => __PACKAGE__,
        type_class  => __PACKAGE__ . 'Type',
    },
  },
  'SQL_TYPE_NAMES',
  'SQL_TYPE_VALUES';

use Types::Standard 'ArrayRef', 'Enum', 'Int', 'InstanceOf';

use constant DataType => Enum->of( grep { !Ref::Util::is_ref $_ } SQL_TYPE_VALUES )
  | InstanceOf ['CXC::DB::DDL::Field::PgType'];

use Moo;

use namespace::clean;

extends 'CXC::DB::DDL::Field';

has '+data_type' => (
    is     => 'ro',
    isa    => ArrayRef->of( DataType )->plus_coercions( DataType, sub { [$_] } ),
    coerce => 1,
);

=method type_name

   $name = $field->type_name( $dbh );

return the type name for the type

=cut

sub type_name ( $self, $dbh ) {

    # if the type is an object, it's guaranteed to be one of ours, so
    # use it directly
    for my $type ( $self->data_type->@* ) {
        return $type->pg_type_name if Scalar::Util::blessed( $type );
    }

    return $self->next::method( $dbh );
}

1;

# COPYRIGHT
